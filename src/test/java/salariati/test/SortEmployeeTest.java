package salariati.test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.EmployeeFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMockRepository;
import salariati.validator.EmployeeValidator;

public class SortEmployeeTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMockRepository();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }


    @Test
    public void sortListTest(){
        try {
            controller.addEmployee(new Employee(1500, "Xander", "Bla", "1970101123123", EmployeeFunction.TEACHER, 3000));
            employeeRepository.sortList();
            assertTrue(employeeRepository.getEmployeeList().get(0).getSalary()==3000);
        }
        catch(EmployeeException e){
            e.printStackTrace();
        }
    }

    @Test
    public void sortListTestInvalid(){
        for(int i=1;i<=6;i++){
            controller.deleteEmployee(controller.getEmployeeById(i));
        }
        try{
            controller.sortList();
        }
        catch(EmployeeException e){
            assertTrue(e.getMessage().equals("Can't sort list because is empty"));
        }
    }
}
