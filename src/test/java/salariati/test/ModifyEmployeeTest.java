package salariati.test;
import static org.junit.Assert.*;

import salariati.controller.EmployeeController;
import salariati.enumeration.EmployeeFunction;

import org.junit.Before;
import org.junit.Test;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMockRepository;
import salariati.validator.EmployeeValidator;

public class ModifyEmployeeTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMockRepository();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void testRepositoryMock() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void testModifyNewEmployee() {
        controller.modifyEmployee(1,EmployeeFunction.TEACHER,2600);
        assertTrue(controller.getEmployeeById(1).getFunction()==EmployeeFunction.TEACHER);
    }

    @Test
    public void modifyInexistentEmploye(){
        try {
            controller.modifyEmployee(123, EmployeeFunction.TEACHER, 3000);
        }
        catch(RuntimeException e){
            assertTrue(e.getMessage().equals("Nu exista employee cu id: 123"));
        }

    }
}
