package salariati.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.internal.configuration.injection.scanner.MockScanner;
import salariati.controller.EmployeeController;
import salariati.enumeration.EmployeeFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMockRepository;
import salariati.validator.EmployeeValidator;
import salariati.view.UserInterface;

import java.io.*;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BigBangIntegrationTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;
    private UserInterface userInterface;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMockRepository();
        controller = new EmployeeController(employeeRepository);
        employeeValidator = new EmployeeValidator();
        userInterface = new UserInterface(controller);

    }

    @Test
    public void f01_test() {
        try {

            Employee newEmployee = new Employee(10, "Alex", "Validlastname", "1910509055057", EmployeeFunction.ASISTENT, 2);
            boolean added = controller.addEmployee(newEmployee);
            assertTrue(added);


        } catch (EmployeeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void f02_test() {
        controller.modifyEmployee(1, EmployeeFunction.TEACHER, 2600);
        assertTrue(controller.getEmployeeById(1).getFunction() == EmployeeFunction.TEACHER);
    }

    @Test
    public void f03_test() {
        try {
            controller.addEmployee(new Employee(1500, "Xander", "Bla", "1970101123123", EmployeeFunction.TEACHER, 4000));
            employeeRepository.sortList();
            assertTrue(employeeRepository.getEmployeeList().get(0).getSalary() == 4000);
        } catch (EmployeeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void integration_test() {

        try {


            userInterface.bufferedReader = Mockito.mock(BufferedReader.class);

            Mockito.when(userInterface.bufferedReader.readLine()).thenReturn("1", "8000", "John", "Doe", "1980101123123", "ASISTENT", "4000",
                    "2", "8000", "TEACHER", "5000", "3", "0");
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            System.setOut(new PrintStream(bo));
            bo.flush();
            userInterface.show();
            String allWrittenLines = new String(bo.toByteArray());

            assertTrue(allWrittenLines.contains("Added"));
            assertTrue(allWrittenLines.contains("function modified to TEACHER"));
            assertTrue(allWrittenLines.contains("8000;John;Doe;1980101123123;TEACHER;5000.0"));
            assertTrue(controller.getEmployeesList().get(0).getSalary() == 5000);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
