package salariati.test;

import static org.junit.Assert.*;

import salariati.exception.EmployeeException;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMockRepository;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.EmployeeFunction;

import java.util.Collections;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeController controller;
	private EmployeeValidator employeeValidator;
	
	@Before
	public void setUp() {
		employeeRepository = new EmployeeMockRepository();
		controller         = new EmployeeController(employeeRepository);
		employeeValidator  = new EmployeeValidator();
	}
	

	
	@Test
	public void tc1_ecp() {
		try {

			Employee newEmployee = new Employee(10, "Alex", "Validlastname", "1910509055057", EmployeeFunction.ASISTENT,2);
			assertTrue(employeeValidator.isValid(newEmployee));
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);


		} catch (EmployeeException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void tc3_ecp(){
		try {
			Employee newEmployee = new Employee(100, "abc123", "Validlastname", "1910509055057", EmployeeFunction.ASISTENT, 24);

			boolean added=controller.addEmployee(newEmployee);
			assertFalse(added);

		}
		catch(EmployeeException e){
			e.printStackTrace();
		}
	}

	@Test
	public void tc4_ecp(){
		try{
			Employee newEmployee = new Employee(12, "Ba", "Validlastname", "1910509055057", EmployeeFunction.ASISTENT, 3000);
			boolean added=controller.addEmployee(newEmployee);
			assertFalse(added);
		}
		catch(EmployeeException e){
			e.printStackTrace();
		}
	}

	@Test
	public void tc6_ecp(){
		try{
			Employee newEmployee = new Employee(13, "Barnutiu", "Validlastname", "1910509055057", EmployeeFunction.ASISTENT, 0);
			boolean added=controller.addEmployee(newEmployee);
			assertFalse(added);
		}
		catch (EmployeeException e){

		}
	}

	@Test
	public void tc1_bva(){
		try{
			Employee newEmployee = new Employee(14, "Jon", "Validlastname", "1910509055057", EmployeeFunction.ASISTENT, 1);
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);
		}
		catch (EmployeeException e){

		}
	}

	@Test
	public void tc4_bva(){
		try{

			Employee newEmployee = new Employee(14, "Jon", "Validlastname", "1910509055057", EmployeeFunction.ASISTENT, Double.MAX_VALUE);
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);
		}
		catch (EmployeeException e){

		}
	}

	@Test
	public void tc5_bva(){
		try{
			Employee newEmployee = new Employee(14, "Jon", "Validlastname", "1910509055057", EmployeeFunction.ASISTENT, Double.MAX_VALUE-1);
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);
		}
		catch (EmployeeException e){

		}
	}


	@Test
	public void tc8_bva(){
		try{
			Employee newEmployee = new Employee(14, "Jon", "Validlastname", "1910509055057", EmployeeFunction.ASISTENT, Double.MAX_VALUE+1);
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);
		}
		catch (EmployeeException e){

		}
	}

}
