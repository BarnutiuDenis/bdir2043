package salariati.main;

import salariati.enumeration.EmployeeFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepository;
import salariati.controller.EmployeeController;
import salariati.view.UserInterface;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia Employeea, salariul de incadrare);
//F02.	 modificarea functiei Employeee (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {
	
	public static void main(String[] args) {
		EmployeeRepository employeeRepository=new EmployeeRepository();
		EmployeeController employeeController=new EmployeeController(employeeRepository);
		UserInterface userInterface=new UserInterface(employeeController);
		userInterface.show();

}

}
