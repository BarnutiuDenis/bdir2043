package salariati.controller;

import java.util.List;

import salariati.enumeration.EmployeeFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	public boolean addEmployee(Employee employee) throws EmployeeException{
		if(getEmployeeById(employee.getId())==null )
			return employeeRepository.addEmployee(employee);
		else
			throw new EmployeeException("ID already exists");
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(int id, EmployeeFunction function,double salary) {
		employeeRepository.modifyEmployee(id,function, salary);
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}

	public Employee getEmployeeById(int id){
		return employeeRepository.getEmployeeById(id);
	}

	public void sortList() throws EmployeeException{
		employeeRepository.sortList();
	}
	
}
