package salariati.repository.interfaces;

import java.util.List;

import salariati.enumeration.EmployeeFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;

public interface EmployeeRepositoryInterface {
	
	boolean addEmployee(Employee employee);
	void deleteEmployee(Employee employee);
	void modifyEmployee(int id, EmployeeFunction function,double salary);
	List<Employee> getEmployeeList();
	Employee getEmployeeById(int id);
	void getDataFromFile();
	void sortList() throws EmployeeException;



}
