package salariati.repository.mock;

import java.util.ArrayList;
import java.util.List;

import salariati.enumeration.EmployeeFunction;

import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepository;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeMockRepository implements EmployeeRepositoryInterface {

    private List<Employee> employeeList;
    private EmployeeValidator employeeValidator;

    public EmployeeMockRepository() {

        employeeValidator = new EmployeeValidator();
        employeeList = new ArrayList<Employee>();

        Employee Ionel = new Employee(1, "Alex", "Pacuraru", "1234567890876", EmployeeFunction.ASISTENT, 2500);
        Employee Mihai = new Employee(2, "Dumitrescu", "Alex", "1234567890876", EmployeeFunction.LECTURER, 2500);
        Employee Ionela = new Employee(3, "Alex", "Ionescu", "1234567890876", EmployeeFunction.LECTURER, 2600);
        Employee Mihaela = new Employee(4, "Alex", "Pacuraru", "1234567890876", EmployeeFunction.ASISTENT, 2500);
        Employee Vasile = new Employee(5, "Alex", "Georgescu", "1234567890876", EmployeeFunction.TEACHER, 2500);
        Employee Marin = new Employee(6, "Alex", "Puscas", "1234567890876", EmployeeFunction.TEACHER, 2500);

        employeeList.add(Ionel);
        employeeList.add(Mihai);
        employeeList.add(Ionela);
        employeeList.add(Mihaela);
        employeeList.add(Vasile);
        employeeList.add(Marin);
    }

    @Override
    public boolean addEmployee(Employee employee) {
        if (employeeValidator.isValid(employee)) {
            employeeList.add(employee);
            return true;
        }
        return false;
    }

    @Override
    public void deleteEmployee(Employee employee) {
        employeeList.remove(employee);
    }


    @Override
    public void modifyEmployee(int id, EmployeeFunction function, double salary) {
        Employee employee = getEmployeeById(id);
        if (employee != null) {
            employee.setFunction(function);
            employee.setSalary(salary);
        } else
            throw new RuntimeException("Nu exista employee cu id: " + id);

    }

    @Override
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    @Override
    public Employee getEmployeeById(int id) {

        for (Employee employee : employeeList) {
            if (employee.getId() == id)
                return employee;
        }
        return null;
    }

    @Override
    public void getDataFromFile() {
        //Not necessary for mock repository
    }

    @Override
    public void sortList() throws EmployeeException {
        if (employeeList.size() == 0)
            throw new EmployeeException("Can't sort list because is empty");
        employeeList.sort(EmployeeRepository::COMPARATOR);

    }

    public void addRaise() throws EmployeeException {
        int sum = 0, index = 0;
        if (employeeList.size() == 0)
            throw new EmployeeException("nu exista angajati");

        while (sum < 10000 && index < employeeList.size()) {
            Employee employee = employeeList.get(index);
            if (employee.getFunction() == EmployeeFunction.TEACHER) {
                employee.setSalary(employee.getSalary() + 500);
                sum += 500;
            } else if (employee.getFunction() == EmployeeFunction.ASISTENT) {
                employee.setSalary(employee.getSalary() + 300);
                sum += 300;
            }
            index++;
        }
    }


}
